#!/usr/bin/env python
# -*- coding: utf-8 -*-


from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
import logging
import urllib.request, json
import pprint

# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)


# Define a few command handlers. These usually take the two arguments bot and
# update. Error handlers also receive the raised TelegramError object in error.
def start(bot, update):
    """Send a message when the command /start is issued."""
    update.message.reply_text('Kerron sinulle stimuilmoista.')


def help(bot, update):
    """Send a message when the command /help is issued."""
    update.message.reply_text('/ilmos - kertoo viimeiset viisi ilmoa ja kokonaismäärän.\n/ilmos `n` - kertoo viimeiset n ilmoa ja kokonaismäärän. Yli 50 defaulttaa normaaliin viestiin.\n/search - etsi ilmoista nimiä.\n\n\nhttps://gitlab.com/omnez/stimuilmobot/')


def formulate_standard_message():
    with urllib.request.urlopen("http://20.as.fi/api/registrations") as url:
        data = json.loads(url.read().decode())
    last_five_ilmos="Viimeiset viisi ilmoittautunutta:"
    i=0        
    while i < len(data["registrations"]):
        if i>=(len(data["registrations"])-5):
            last_five_ilmos= last_five_ilmos + "\n" + str(data["registrations"][i]["name"])
        i = i+1
    return last_five_ilmos + "\nIlmoittautuneita yhteensä: " + str(len(data["registrations"]))

def formulate_custom_message(amount):
    with urllib.request.urlopen("http://20.as.fi/api/registrations") as url:
        data = json.loads(url.read().decode())
    last_ilmos="Viimeiset " + str(amount) + " ilmoittautunutta:"
    i=0
    while (i+1) <= len(data["registrations"]):
        if i>=(len(data["registrations"])-int(amount)):
            last_ilmos= last_ilmos + "\n" + str(data["registrations"][i]["name"])
        i = i+1

    return last_ilmos + "\nIlmoittautuneita yhteensä: " + str(len(data["registrations"]))  


def ilmos(bot, update, args):
    if not args:
        update.message.reply_text(formulate_standard_message())
        print(str(update.message.chat.title) + " - " + str(update.message.from_user.username))
    else:
        try:
            int(args[0])
            if int(args[0]) < 1 or int(args[0]) > 50:
                update.message.reply_text("Anna parametriksi 1-50!")
                update.message.reply_text(formulate_standard_message())
                print(str(update.message.chat.title) + " - " + str(update.message.from_user.username))
            else:
                update.message.reply_text(formulate_custom_message(args[0]))
                print(str(update.message.chat.title) + " - " + str(update.message.from_user.username))
        except ValueError:
            update.message.reply_text("Parametriksi kelpaa vain numero!\nMuista /search-komento.")
            update.message.reply_text(formulate_standard_message())
            print(str(update.message.chat.title) + " - " + str(update.message.from_user.username))



def search(bot, update, args):
    if not args:
        update.message.reply_text("Anna hakutermi. (min. 3 merkkiä)")
    else:
        with urllib.request.urlopen("http://20.as.fi/api/registrations") as url:
            data = json.loads(url.read().decode())
            result_message="\n"
            result_amount = 0
            if(len(str(args[0]))<3):
                update.message.reply_text("Liian lyhyt hakutermi!")
            else:
                for result in data["registrations"]:
                    if(result['name'].upper().find(str(args[0]).upper()) != -1):
                        result_amount = result_amount + 1
                        result_message = result_message + str(result['name']) + "\n"

                if(result_amount < 1):
                    update.message.reply_text("Ei hakutuloksia")

                update.message.reply_text("Hakua vastasi " + str(result_amount) + " ilmoa:" + result_message)

        print(str(update.message.chat.title) + " - " + str(update.message.from_user.username))


def error(bot, update, error):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, error)


def main():
    """Start the bot."""
    # Create the EventHandler and pass it your bot's token.
    token = open('token.txt').read().strip()
    updater = Updater(token)

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    # on different commands - answer in Telegram
    dp.add_handler(CommandHandler("start", start))
    dp.add_handler(CommandHandler("help", help))
    dp.add_handler(CommandHandler("ilmos", ilmos, pass_args=True))
    dp.add_handler(CommandHandler("search", search, pass_args=True))



    # log all errors
    dp.add_error_handler(error)

    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    main()
